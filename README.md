## 半成品...之前没怎么用过typescript，有点累。。

---
## 大概思路
1.用koa创建一个http服务，用来做用户管理，用户登录后生成一个token
2.登录成功后给一个链接，token作为url参数携带
3.可以将这个链接发送给想共享账号的人，同一token就使用同一cookie
4.链接对应的代理服务（本来想合成一个服务的，但是在koa里弄了半天感觉头大了，就另外在起的一个服务）
5.访问链接时会将token写到cookie里，并且将token和cookie写入mysql
6.当有代理服务接到其他请求时从cookie中取出token，查找出对应的cookie，修改请求头的cookie，
  并在处理代理请求后更新token对应的cookie

## 之前用原生的node.js做了个原型，换成koa和typescript的过程不太顺畅。。。眼睛好累了，缓一下。。。
