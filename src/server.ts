import * as Koa from "koa";
import * as Router from "koa-router";
import * as BodyParser from "koa-bodyparser";
import * as httpProxy from "http-proxy";
import * as https from "https";
import * as http from "http";
import * as url from "url";
const app = new Koa();
const router = new Router();

app.use(BodyParser());

import * as userController from "./controllers/user";
import * as cookieController from "./controllers/cookie";
import cookieModel from "./models/cookie";

router.get("/login", userController.getLogin);
router.post("/login", userController.postLogin);
router.get("/signup", userController.getSignup);
router.post("/signup", userController.postSignup);
router.get("/logout", userController.logout);

router.post("/newcookie", cookieController.postNew);
router.get("/deletecookie", cookieController.getDelete);
// router.get("/*", cookieController.shareCookie);
// router.post("/*", cookieController.shareCookie);

app.use(router.routes());

app.listen(3000);

console.log("Server running on port 3000");

const proxy = httpProxy.createProxyServer({});
const server2 = http.createServer(async function(req, res) {
  let urlObj = url.parse(req.url, true);
  let model = new cookieModel();
  if (urlObj.query.token) {
    const token: string = <string>urlObj.query.token;
    let cookie = await model.getCookieByToken(token);
    if (cookie) {
      req.headers.cookie =
        cookie.replace(/token=.*?[;\b]/i, "") + `token=${token}`;
    }
  }
  const target = "https://www.4008-517-517.cn";
  const parsedUrl = url.parse(target);
  proxy.web(req, res, {
    target: target,
    agent: https.globalAgent,
    headers: {
      host: parsedUrl.hostname
    },
    prependPath: false,
    xfwd: true,
    autoRewrite: true,
    protocolRewrite: "http"
  });
  console.log(urlObj.query.token + "=====" + req.headers.cookie);
  await model.add(<string>urlObj.query.token, <string>req.headers.cookie);
});

server2.listen(3030);
console.log("Proxy running on port 3030");
