import BaseModel from "./base";

class User extends BaseModel {
  id: number;
  name: string;
  pass: string;
  add = async function(name: string, pass: string) {
    return await this.do(
      `INSERT INTO user (name, pass) VALUES ('${name}', '${pass}')`
    );
  };
  getPassByName = async function(name: string) {
    const pass = await this.do(`select pass from user where name = '${name}'`);
    return pass[0].pass;
  };
}

export default User;
