import BaseModel from "./base";

class Cookie extends BaseModel {
  token: string;
  cookie: string;
  add = async function(token: string, cookie: string) {
    console.log("add=======");
    if (this.getCookieByToken(token)) {
      console.log("1");
      return await this.do(
        `update cookie set cookie = '${cookie}' where token = '${token}'`
      );
    } else {
      console.log("2");
      return await this.do(
        `INSERT INTO cookie (token, cookie) VALUES ('${token}', '${cookie}')`
      );
    }
  };
  getCookieByToken = async function(token: string) {
    const cookie = await this.do(
      `select cookie from cookie where token = '${token}'`
    );
    if (cookie.length > 0) {
      return cookie[0].cookie;
    } else {
      return false;
    }
  };
}

export default Cookie;
