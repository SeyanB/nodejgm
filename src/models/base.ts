import * as Mysql from "mysql";

const mysqlPool = Mysql.createPool({
  connectionLimit: 10,
  host: "localhost",
  user: "bjw",
  password: "PASSWORD",
  database: "test_db",
  insecureAuth: true
});
class BaseModel {
  static pool: Mysql.Pool = mysqlPool;
  public do(sql: string, values?: any[]) {
    return new Promise((resolve, reject) => {
      BaseModel.pool.getConnection(function(err, connection) {
        if (err) {
          reject(err);
        } else {
          connection.query(sql, values, (err, rows) => {
            if (err) {
              reject(err);
            } else {
              resolve(rows);
              connection.release();
            }
          });
        }
      });
    });
  }
}

export default BaseModel;
