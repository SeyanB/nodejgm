import cookieModel from "../models/cookie";
import * as httpProxy from "http-proxy";
import * as https from "https";
import * as url from "url";
import * as Koa from "koa";

let proxy = httpProxy.createProxyServer({});

export const postNew = async function(
  ctx: Koa.Context,
  next: Koa.Middleware
) {};

export const getDelete = async function(
  ctx: Koa.Context,
  next: Koa.Middleware
) {};
export const shareCookie = async function(
  ctx: Koa.Context,
  next: Koa.Middleware
) {
  let token = ctx.cookies.get("token") || undefined;
  let target = "https://www.4008-517-517.cn";
  let parsedUrl = url.parse(target);
  if (token) {
    let cookie = (<any>ctx.req.headers.cookie).replace(/token=.*?[;\b]/i, "");
    let model = new cookieModel();
    await model.add(token, cookie);
    proxy.web(ctx.req, ctx.res, {
      target: target,
      agent: https.globalAgent,
      headers: {
        host: parsedUrl.hostname
      },
      changeOrigin: false,
      xfwd: true,
      prependPath: false,
      ignorePath: true,
      autoRewrite: true,
      protocolRewrite: "http"
    });
    console.log(ctx.req.headers.host);
    ctx.url = "127.0.0.1:3000";
    ctx.respond = false;
  } else {
    let token = ctx.query.token;
    let model = new cookieModel();
    let cookie = await model.getCookieByToken(token);
    ctx.req.headers.cookie = cookie;
    proxy.web(ctx.req, ctx.res, {
      target: target,
      agent: https.globalAgent,
      headers: {
        host: parsedUrl.hostname
      },
      prependPath: false,
      ignorePath: true,
      autoRewrite: true,
      xfwd: true,
      protocolRewrite: "http"
    });
    ctx.respond = false;
  }
};
