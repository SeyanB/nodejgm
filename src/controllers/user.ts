import userModel from "../models/user";
import * as Koa from "koa";

const chars = [
  "0",
  "1",
  "2",
  "3",
  "4",
  "5",
  "6",
  "7",
  "8",
  "9",
  "A",
  "B",
  "C",
  "D",
  "E",
  "F",
  "G",
  "H",
  "I",
  "J",
  "K",
  "L",
  "M",
  "N",
  "O",
  "P",
  "Q",
  "R",
  "S",
  "T",
  "U",
  "V",
  "W",
  "X",
  "Y",
  "Z"
];

function generateMixed(n: number) {
  let res = "";
  for (let i = 0; i < n; i++) {
    let id = Math.ceil(Math.random() * 35);
    res += chars[id];
  }
  return res;
}

export const getLogin = async function(ctx: Koa.Context, next: Koa.Middleware) {
  ctx.response.body =
    '<h1>Login</h1> <form action="/login" method="post"> ' +
    '<p>Name: <input name="name"></p>' +
    ' <p>Password: <input name="password" type="password"></p> ' +
    '<p><input type="submit" value="Submit"><a href="/signup">signup</a></p>' +
    " </form>";
};
export const postLogin = async function(
  ctx: Koa.Context,
  next: Koa.Middleware
) {
  const name: string = (<any>ctx.request.body).name || "",
    password: string = (<any>ctx.request.body).password || "";
  const model = new userModel();
  const realPass = await model.getPassByName(name);
  if (password == realPass) {
    let token = generateMixed(6);
    ctx.cookies.set("token", token);
    ctx.body = `<h1>LOgin Success</h1><p><a href="http://127.0.0.1:3030?token=${token}" target="_Blank">sharecookie</a></p>`;
  } else {
    ctx.body = "Login error!Wrong password";
  }
};

export const getSignup = async function(
  ctx: Koa.Context,
  next: Koa.Middleware
) {
  ctx.response.body =
    '<h1>Signup</h1> <form action="/signup" method="post"> ' +
    '<p>Name: <input name="name"></p>' +
    ' <p>Password: <input name="password" type="password"></p> ' +
    '<p><input type="submit" value="Submit"><a href="/login">signup</a></p>' +
    " </form>";
};
export const postSignup = async function(
  ctx: Koa.Context,
  next: Koa.Middleware
) {
  const name: string = (<any>ctx.request.body).name || "",
    password: string = (<any>ctx.request.body).password || "";
  const model = new userModel();
  const result = await model.add(name, password);

  if (result) {
    ctx.response.body = '<h1>succes！</h1><a href="/login">login</a>';
  } else {
    ctx.response.body =
      "<h1>fail！</h1>" +
      `<p>Info: ${result.info}</p>` +
      '<p><a href="/login">login</a><a href="/login">signup</a></p>';
  }
};

export const logout = async function(ctx: Koa.Context, next: Koa.Middleware) {};
